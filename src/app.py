import os

from flask import Flask, request, g

from db import db
from dto import BookDto
from utils import filter_data_by_var

app = Flask(__name__)

PORT: int = int(os.environ.get('PORT', 8000))


@app.before_request
def before_request():
    if getattr(g, 'books', None) is None:
        g.books = db


@app.route('/books', methods=['GET'])
def get_all():
    books = getattr(g, 'books', [])
    args = request.args
    author = args.get('author')
    year = args.get('year')
    genre = args.get('genre')

    if len(books) > 0:
        books = filter_data_by_var(books, author=author, genre=genre, year=year)

    return books


@app.route('/books/<id>', methods=['GET'])
def get_by_id(uid):
    books = getattr(g, 'books', [])

    if len(books) > 0:
        books = filter_data_by_var(books, id=uid)

    if len(books) > 0:
        return books[0]


@app.route('/books', methods=['POST'])
def create():
    books = getattr(g, 'books', [])
    last_id = int(books[-1].get('id', '0'))
    new_book_dto = BookDto(**request.json)
    new_book = {
        'id': str(last_id + 1),
        'title': new_book_dto.title,
        'author': new_book_dto.author,
        'year': new_book_dto.year,
        'genre': new_book_dto.genre
    }
    g.books.append(new_book)
    return new_book


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=PORT)
