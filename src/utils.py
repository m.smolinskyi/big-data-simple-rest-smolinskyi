def filter_data_by_var(data_list, **kwargs):
    for key in kwargs.keys():
        if kwargs[key] is not None:
            filtered_list = [item for item in data_list if str(item.get(key)) == kwargs[key]]
            data_list = filtered_list
    return data_list
