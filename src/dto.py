from pydantic import BaseModel, PositiveInt

class BookDto(BaseModel):
    title: str
    author: str
    year: PositiveInt
    genre: str
